import torch.nn as nn
import torch.nn.functional as F


class TextRecognitionModel(nn.Module):
    def __init__(self, input_channels, num_classes):
        super(TextRecognitionModel, self).__init__()
        self.conv_layer = nn.Sequential(
            nn.Conv2d(input_channels, 64, kernel_size=3, padding=1),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2, stride=2),
            nn.Conv2d(64, 128, kernel_size=3, padding=1),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2, stride=2),
            nn.Conv2d(128, 256, kernel_size=3, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(256, 256, kernel_size=3, padding=1),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=(2, 1), stride=(2, 1)),
            nn.Conv2d(256, 512, kernel_size=3, padding=1),
            nn.BatchNorm2d(512),
            nn.ReLU(inplace=True),
            nn.Conv2d(512, 512, kernel_size=3, padding=1),
            nn.BatchNorm2d(512),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=(2, 1), stride=(2, 1)),
            nn.Conv2d(512, 512, kernel_size=2, padding=0),
        )
        self.rnn_layer = nn.Sequential(
            nn.LSTM(
                input_size=512,
                hidden_size=256,
                num_layers=2,
                batch_first=True,
                bidirectional=True
            ),
        )
        self.fc = nn.Linear(512, num_classes)

    def forward(self, x):
        x = self.conv_layer(x)
        x = x.squeeze(2)
        x = x.permute(0, 2, 1)
        x, _ = self.rnn_layer(x)
        x = self.fc(x)
        return x
