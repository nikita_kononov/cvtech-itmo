import argparse
from itertools import groupby
import os
import time
import typing

import torch
from torch.utils.data import DataLoader

from dataset import CaptchaDataset
from model import TextRecognitionModel


def load_model(model, checkpoint_path: str):
    checkpoint = torch.load(
        checkpoint_path, map_location=lambda storage, loc: storage,
    )
    model_state_dict = model.state_dict()
    new_state_dict = {
        k: v
        for k, v in checkpoint["state_dict"].items()
        if k in model_state_dict.keys()
    }

    model_state_dict.update(new_state_dict)
    model.load_state_dict(model_state_dict)

    print("Loaded pretrained model weights.")


def get_dataloaders() -> typing.Tuple[int, int, DataLoader]:
    test_ds = CaptchaDataset((2, 6), samples=1000)

    test_dataloader = DataLoader(
        dataset=test_ds,
        batch_size=1,
        shuffle=False,
        num_workers=1,
        pin_memory=True,
        drop_last=False,
        persistent_workers=True
    )
    return (
        test_ds.num_classes,
        test_ds.blank_label,
        test_dataloader
    )

def prepare_out(y, blank):
    y = y.to(torch.int32)
    return y[y != blank]

def is_correct(prediction, y_true, blank):
    prediction = prepare_out(prediction, blank)
    y_true = prepare_out(y_true, blank)
    return len(prediction) == len(y_true) and torch.all(prediction.eq(y_true))


def main(args):
    num_classes, blank, test_dataloader = get_dataloaders()

    os.makedirs('checkpoints', exist_ok=True)
    device = torch.device('cpu' if args.cpu else 'cuda')

    model = TextRecognitionModel(1, num_classes).to(device)
    assert args.checkpoint
    load_model(model, args.checkpoint)

    model.eval()
    correct = total = 0
    with torch.no_grad():
        while True:
            total += 1
            for x_val, y_true in test_dataloader:
                break
            x_val = x_val.unsqueeze(1) # [B,Ch,H,W] (64,1,32,140)
            x_val = x_val.to(device=device)
            y_true = y_true.to(device=device) # [B,LenSeq] (64, 5)

            y_pred = model(x_val) # [B,?, NumClass+1] (64, 34, 11)
            y_pred = y_pred.permute(1, 0, 2)

            _, max_index = torch.max(y_pred, dim=2)

            raw_prediction = list(max_index[:, 0].detach().cpu().numpy())
            prediction = torch.IntTensor(
                [c for c, _ in groupby(raw_prediction) if c != blank]
            ).to(device)
            correct += int(is_correct(prediction, y_true[0], blank))
            gt = prepare_out(y_true, blank)
            print(
                f'Result!\n'
                f'\tGround Truth:\t{gt.tolist()}\n'
                f'\tPredicted:\t{prediction.tolist()}\n'
                f'\tTotal correct:\t{correct} / {total}\n'
            )
            time.sleep(0.25)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog='Inferencer',
        description='Args to run ocr-model inference',
        epilog='Good luck!'
    )
    parser.add_argument('-c', '--checkpoint', type=str)
    parser.add_argument('--cpu', action='store_true')

    args = parser.parse_args()
    main(args)
