import argparse
from itertools import groupby
import os
import typing

import torch
from torch.functional import F
import torch.nn as nn
from torch.utils.data import DataLoader
import tqdm

from dataset import CaptchaDataset
from model import TextRecognitionModel
import nltk


def load_model(model, optimizer, checkpoint_path: str):
    checkpoint = torch.load(
        checkpoint_path, map_location=lambda storage, loc: storage,
    )
    model_state_dict = model.state_dict()
    new_state_dict = {
        k: v
        for k, v in checkpoint["state_dict"].items()
        if k in model_state_dict.keys()
    }

    model_state_dict.update(new_state_dict)
    model.load_state_dict(model_state_dict)
    optimizer.load_state_dict(checkpoint["optimizer"])

    print("Loaded pretrained model weights.")


def save_model(model, optimizer, checkpoint_path: str):
    torch.save(
        {
            "state_dict": model.state_dict(),
            "optimizer": optimizer.state_dict(),
        },
        checkpoint_path,
    )


def get_dataloaders(
        batch_size: int, num_workers: int
) -> typing.Tuple[int, int, DataLoader, DataLoader]:
    train_ds = CaptchaDataset((4, 5))
    test_ds = CaptchaDataset((2, 6), samples=1000)

    train_dataloader = DataLoader(
        dataset=train_ds,
        batch_size=batch_size,
        shuffle=True,
        num_workers=num_workers,
        pin_memory=True,
        drop_last=True,
        persistent_workers=True
    )

    test_dataloader = DataLoader(
        dataset=test_ds,
        batch_size=1,
        shuffle=False,
        num_workers=1,
        pin_memory=True,
        drop_last=False,
        persistent_workers=True
    )
    return (
        train_ds.num_classes,
        train_ds.blank_label,
        train_dataloader,
        test_dataloader
    )


def prepare_out(y, blank):
    y = y.to(torch.int32)
    return y[y != blank]

def calc_metrics(prediction, y_true, blank):
    prediction = prepare_out(prediction, blank)
    y_true = prepare_out(y_true, blank)
    acc = len(prediction) == len(y_true) and torch.all(prediction.eq(y_true))
    if len(y_true) == 0:
        cer = 0.0
    else:
        distance = nltk.edit_distance(prediction.cpu().numpy(), y_true.cpu().numpy())
        cer = distance / len(y_true)
    return acc, cer


def train(model, train_loader, criterion, optimizer, epoch: int, device):
    model.train()

    for x_train, y_true in tqdm.tqdm(train_loader):
        x_train = x_train.unsqueeze(1) # [B,Ch,H,W] (64,1,32,140)
        bs, _, height, _ = x_train.shape
        optimizer.zero_grad()
        x_train = x_train.to(device=device)
        y_true = y_true.to(device=device) # [B,LenSeq] (64, 5)

        y_pred = model(x_train) # [B,?, NumClass+1] (64, 34, 11)
        y_pred = y_pred.permute(1, 0, 2)
        input_lengths = torch.IntTensor(bs).fill_(height)
        target_lengths = torch.IntTensor([len(t) for t in y_true])
        loss = criterion(y_pred, y_true, input_lengths, target_lengths)
        loss.backward()
        optimizer.step()


def val(model, dataloader, epoch: int, device, blank) -> float:
    model.eval()

    with torch.no_grad():
        val_correct = 0
        val_cer = 0
        val_total = 0
        for x_val, y_true in tqdm.tqdm(dataloader):
            x_val = x_val.unsqueeze(1) # [B,Ch,H,W] (64,1,32,140)
            bs, _, height, _ = x_val.shape
            x_val = x_val.to(device=device)
            y_true = y_true.to(device=device) # [B,LenSeq] (64, 5)

            y_pred = model(x_val) # [B,?, NumClass+1] (64, 34, 11)
            y_pred = y_pred.permute(1, 0, 2)

            _, max_index = torch.max(y_pred, dim=2)
            for i in range(bs):
                raw_prediction = list(max_index[:, i].detach().cpu().numpy())
                prediction = torch.IntTensor(
                    [c for c, _ in groupby(raw_prediction) if c != blank]
                ).to(device)
                acc, cer = calc_metrics(prediction, y_true[i], blank)
                if acc:
                    val_correct += 1
                val_cer += cer
                val_total += 1
        acc = val_correct / val_total
        print(
            f"Validation\t"
            f"\tAccuracy: {val_correct} / {val_total} = {acc}"
            f"\tCER: {cer / val_total}"
        )
    return acc


def main(args):
    num_classes, blank, train_dataloader, test_dataloader = get_dataloaders(
        args.batch_size, args.num_workers
    )

    os.makedirs('checkpoints', exist_ok=True)
    device = torch.device('cpu' if args.cpu else 'cuda')

    model = TextRecognitionModel(1, num_classes).to(device)
    optimizer =  torch.optim.Adadelta(model.parameters(), rho=0.9)
    if args.checkpoint:
        load_model(model, optimizer, args.checkpoint)

    criterion = nn.CTCLoss(blank=blank)
    criterion = criterion.to(device=device)

    best_acc = 0.0

    for epoch in range(args.epochs):
        train(model, train_dataloader, criterion, optimizer, epoch, device)
        acc = val(model, test_dataloader, epoch, device, blank)

        if acc > best_acc:
            save_model(model, optimizer, f'checkpoints/cp_{epoch}.pt')
            best_acc = acc


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog='Trainer',
        description='Args to run ocr-model training',
        epilog='Good luck!'
    )
    parser.add_argument(
        '-c', '--checkpoint', type=typing.Optional[str], default=None,
    )
    parser.add_argument('-b', '--batch-size', type=int, default=64)
    parser.add_argument('-w', '--num-workers', type=int, default=4)
    parser.add_argument('-e', '--epochs', type=int, default=300)
    parser.add_argument('--cpu', action='store_true')

    args = parser.parse_args()
    main(args)
